import java.sql.*;

public class Myjava {
    public static void main(String arg[])
    {
        Connection connection = null;
        try {
            // below two lines are used for connectivity.
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/databases",
                    "root", "root");

            // mydb is database
            // mydbuser is name of database
            // mydbuser is password of database

            Statement statement;
            statement = connection.createStatement();
            ResultSet resultSet;
            resultSet = statement.executeQuery(
                    "select * from empdetails");
            int empid;
            String empname;
            String empadd;
            Date empdate;
            while (resultSet.next()) {
                empid = resultSet.getInt("empid");
                empname = resultSet.getString("empname").trim();
                empadd = resultSet.getString("empadd").trim();
                empdate = resultSet.getDate("empdate");
                System.out.println("empid : " + empid
                        + " empname : " + empname
                + " empadd: "+ empadd
                + " empdate: "+ empdate);
            }
            resultSet.close();
            statement.close();
            connection.close();
        }
        catch (Exception exception) {
            System.out.println(exception);
        }
    } // function ends
} // class ends

