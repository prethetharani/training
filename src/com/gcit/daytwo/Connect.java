import java.sql.*;

public class Connect {
    public static void main(String arg[])
    {
        Connection connection = null;
        try {
            // below two lines are used for connectivity.
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/data",
                    "root", "root");
            System.out.println(connection);

            // mydb is database
            // mydbuser is name of database
            // mydbuser is password of database

            Statement statement;
            statement = connection.createStatement();
            ResultSet resultSet;
            resultSet = statement.executeQuery(
                    "select * from student");
            int id;
            String name;
            String address;
            while (resultSet.next()) {
                System.out.println("inside while");
                id = resultSet.getInt("id");
                name = resultSet.getString("name").trim();
                address= resultSet.getString("address").trim();
                System.out.println("id : " + id
                        + " name : " + name
                        +" address:"+address);
            }
            resultSet.close();
            statement.close();
            connection.close();
        }
        catch (Exception exception) {
            System.out.println(exception);
        }
    } // function ends
} // class ends
