package taskone;
class Animal{
    void eat(){System.out.println("eating");}
}
class Dog extends Animal{
    void bark(){System.out.println("bark");}
}
class Cat extends Animal{
    void meow(){
        System.out.println("meow");
    }
}
class Inheritance{
    public static void main(String args[]){
        Dog d=new Dog();
        Cat c=new Cat();
        d.bark();
        d.eat();
        c.meow();
        c.eat();
    }}
